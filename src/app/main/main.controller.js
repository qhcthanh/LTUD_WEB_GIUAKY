(function() {
    'use strict';

    angular
        .module('cv')
        .controller('MainController', MainController);

    /** @ngInject */
    function MainController($timeout, $scope, $mdDialog, $mdMedia) {
        var vm = this;
        vm.form = true;       

        vm.person = {
          firstName : "Quach Ha Chan" ,
          lastName : "Thanh",
          fullName: "Quách Hà Chấn Thành",
          email: "qhcthanh@gmail.com",
          major: "Mobile eveloper",
          imageSrc: "https://scontent-sit4-1.xx.fbcdn.net/hphotos-xlf1/v/t1.0-9/6037_909933309123800_6443346368975194098_n.jpg?oh=b4d7fd062201f72f714f78f0b734c1d6&oe=57BC51FF",
          birthday: "09/12/1995",
          address: "205/25 Âu Cơ, Phường 5, Quận 11, TP Hồ Chí Minh, Việt Nam.",
          phone: "+1863210265",
          fb: "https://www.facebook.com/qhcthanh",
          gg: "https://plus.google.com/+ChanThanh95/",
          tw: "https://twitter.com/qhcthanh912",
          yt: "https://www.youtube.com/channel/UCedBKKRgLGeQT5wRUrEHMGg",
          link: "https://www.linkedin.com/in/quach-ha-chan-thanh-669818117",
          skype: "qhcthanh@gmail.com",
          sologan: "Sống là phải phấn đầu hết mình",
        };
        vm.activity = [
          {
             name : "Mobile Club - KHTN",
             org : "ĐH Khoa Học Tự Nhiên HCM",
            time: "10/2015",
            img: "https://scontent-sit4-1.xx.fbcdn.net/hphotos-xta1/v/t1.0-9/12118728_899450030148783_2729901023384213338_n.png?oh=9ba55e8d38fea78660779dbcd948d441&oe=578492A2"
          },
          {
            name: "Cuộc thi Coding Chanllenge - AISC",
            org: "VSEE - ĐH Khoa Học Tự Nhiên HCM",
            time: "10/2015",
            img: "https://scontent-sit4-1.xx.fbcdn.net/hphotos-xpt1/v/t1.0-9/1610984_10153758771112053_6779621277928793639_n.jpg?oh=fd631d98d72adeeaece56d1d179ef8bb&oe=5780FBC5"
          }
        ];
        vm.schools = [
          {
            img: "http://vinastemcelllab.com/vi/images/stories/LOGO_KHTN.png",
            name: "Trường Đại Học Khoa Học Tự Nhiên, TPHCM",
            shortName: "Khoa Học Tự Nhiên",
            time: "2013 - 2017",
            notes: "Khoa Công Nghệ Thông Tin - Hệ Đại Học - Chính Quy",
          },
          {
            img: "https://crunchbase-production-res.cloudinary.com/image/upload/c_pad,h_140,w_140/v1437636764/spmw2bsxysnzmbjbsiwp.png" ,
            name: "Khoa Phạm Trainning",
            shortName: "Khoa Phạm",
            time: "10/2015 - 01/2016",
            notes: "Khoa học lập trình cơ bản về Android",
          },
          {
            name: "VIETNAM ANDROID ACADEMY",
            shortName: "VIETNAM ANDROID",
            notes: "Khoá học lập trình cơ bản về Android",
            img: "http://photos3.meetupstatic.com/photos/event/8/e/e/2/event_444456578.jpeg",
            time: "11/2015 - 12/2015",
          },
        ]; 
        vm.strongSkills = [
          {
            skill: "Swift/Objective-C",
            exprensive: "1 năm",
            notes: "Sử dụng để làm những sản phẩm thật như Let's Match, English-Germany Dictionary, Meetmii tại The Simple Studio",
            img: "http://www.wired.com/wp-content/uploads/2014/07/Apple_Swift_Logo.png",
          },
          {
            skill: "Android/Java",
            exprensive: "6 tháng",
            notes: "Được học từ các khoá học ngoài trường nắm vững các kiến thức cơ bản về android, sử dụng làm sản phẩm MyNearbyMap",
            img: "http://www.technoburgh.com/wp-content/uploads/2015/01/android-logo.jpg",
          },
          {
            skill: "C# - MVVM - MVC",
            exprensive: "1 năm",
            notes: "Sử dụng để làm đồ án các môn học như lập trình window, phân tích thiết kế. Tham gia cuộc thi AISC do đại sứ quán Hoa Kỳ tổ chứ (VSSE)",
            img: "http://m.img.brothersoft.com/iphone/1233/498725233_icon175x175.jpg",
          },
          {
            skill: "C/C++",
            exprensive: "2 năm",
            notes: "Sử dụng để làm đồ án các môn học ở trường năm 1, năm 2",
            img: "http://a5.mzstatic.com/us/r30/Purple7/v4/0d/aa/2d/0daa2d82-2d6d-3c6d-f5b0-7dad7fec2fcb/icon175x175.jpeg",
          },
        ];
        vm.supportSkills = [
          {
            skill: "Firebase",
            exprensive: "3 tháng",
            notes: "Xây dựng ứng dụng chat. Ứng dụng đặt lịch hẹn trên iOS (Swift)",
            img: "https://s3.amazonaws.com/ionic-marketplace/firebase-seed/icon.png",
          },
          {
            skill: "MS SQL",
            exprensive: "1 năm",
            notes: "Nắm vững kiến thức ở môn cơ sở dữ liệu. Sử dụng tại làm cơ sở dữ liệu cho cuộc thi AISC",
            img: "http://mysql.filerepairtool.net/blog/wp-content/uploads/2014/05/microsoft-sql.jpg",
          },
          {
            skill: "SQLite",
            exprensive: "1 năm",
            notes: "",
            img: "http://www.macupdate.com/images/icons256/38584.png",
          },
          {
            skill: "HTML/CSS",
            exprensive: "1 năm",
            notes: "",
            img: "http://www.countable.ca/images/howto-html5-css3.png",
          },
          {
            skill: "Wordpress",
            exprensive: "1 năm",
            notes: "Làm tại ltech với vai trò cộng tác viên hỗ trợ khách hàng sử dụng",
            img: "http://www.fancyicons.com/free-icons/103/socialmedia2-shadow/png/256/wordpress_256.png",
          },
          {
            skill: "Angular",
            exprensive: "3 tháng",
            notes: "",
            img: "https://worldvectorlogo.com/logos/angular-icon.svg",
          },
          {
            skill: "Xamrin",
            exprensive: "3 tháng",
            notes: "",
            img: "https://blog.xamarin.com/wp-content/uploads/2015/03/RDXWoY7W_400x400.png",
          },
        ];
        vm.softSkills = [
          {
            skill: "Tiếng Anh",
            exprensive: "TOEIC 450",
            notes: "",
            img: "http://hotroontap.com/wp-content/uploads/2015/08/14042071097600.png",
          },
          {
            skill: "Tiếng Nhật",
            exprensive: "Sơ cấp phổ thông 2",
            notes: "",
            img: "http://is3.mzstatic.com/image/pf/us/r30/Purple2/v4/ef/b1/e5/efb1e589-7625-fbf4-5e8e-00d02776ab63/pr_source.png",
          },
          {
            skill: "Làm việc nhóm",
            exprensive: "3 năm",
            notes: "3 năm tại trường và làm việc thực tập ở nhiều nơi",
            img: "http://a5.mzstatic.com/us/r30/Purple49/v4/e6/df/63/e6df63f8-ebb7-3ccb-eee2-4ae55f7879ce/icon400x400.jpeg",
          },
        ];
        vm.cerifications = [
          {
            name: "CERTIFICATE OF COMPLETION FOR BEGINER ANDROID TRAINING OF VIETNAM ANDROID - GOOGLE DEVELOPER",
            org: "VIETNAM ANDROID - GOOGLE DEVELOPER",
            time: "12/2015",
          },
          {
            name: "CERTIFICATE OF COMPLETION FOR FIREBASE IN SWIFT",
            org: "VTC ACCADEMY",
            time: "03/2016",
          }
        ];
        vm.schoolExprensive = [
          {
            skill: "C/C++",
            title: "",
            notes: "",
            image: "",
            time: "10/2013 - 7/2014",
            project: {
              projectName: "",
              teamSize: "",
              postion: "",
              description: "",
              time: "",
              product: "",
            },
          },
          {
            skill: "C#",
            title: "",
            notes: "",
            image: "",
            time: "1/2014",
            project: {
              projectName: "",
              description: "",
              time: "",
              video: "",
            },
          },
          {
            skill: "iOS",
            title: "",
            notes: "",
            image: "",
            time: "01/2014",
            project: {
              projectName: "",
              description: "",
              time: "",
              video: "",
            },
          },
          {
            skill: "Android",
            title: "",
            notes: "",
            image: "",
            time: "10/2013 - 7/2014",
            project: {
              projectName: "",
              description: "",
              time: "",
              video: "",
            },
          },
        ];
        vm.workExprensive = [
          {
            skill: "C/C++",
            title: "",
            notes: "",
            image: "",
            time: "10/2013 - 7/2014",
            project: {
              projectName: "",
              description: "",
              time: "",
              video: "",
            },
          },
        ];
        vm.year = Math.floor(2016);
    vm.destination = [
      {
        year: "2016",
        img : "http://www.fastweb.com/uploads/article_photo/photo/2035687/crop635w_2016-Scholarships.jpg",
        name: "Cũng cố kiến thức",
        dest: "Hoàn thành các môn yêu cầu của ngành với điểm trên 7 (Phát triển ứng dụng di động, phát triển ứng dụng web, Lập trình ứng dụng java). Tìm kiếm một công ty để thực tập về iOS/Android. Cố gắng hoàn thành thật nhiều sản phẩm để đưa lên Appstore",
      },
      {
        year: "2017",
        img : "http://insidebitcoins.com/wp-content/uploads/2015/02/2017.jpg",
        name: "Tốt nghiệp",
        dest: "Làm luận văn để tốt nghiệp. Học thêm một số môn bổ trợ (Lập trình game, Thiết kế giao diện, Đặc tả hình thức, Các mẫu thiết kế hướng đối tượng)",
      },
      {
        year: "2018",
        img : "https://chronicle-assets.s3.amazonaws.com/7/img/photos/biz/photo_61153_landscape_650x433.jpg",
        name: "Cũng cố sự nghiệp",
        dest: "Ứng tuyển vào công ty trung bình hoặc lớn với vị trí mobile developer (iOS/Android). Phát triển ứng dụng hoặc game cho ra sản phẩm phục vụ cho người Việt",
      },
      {
        year: "2019",
        img : "http://img.ccrd.clearchannel.com/media/mlib/2088/2015/08/default/2019_0_1440676558.jpg",
        name: "Cũng cố sự nghiệp",
        dest: "Có ít nhất 10 sản phẩm trên Appstore. Tổng các ứng dụng phải trên 5000 người download. Xem xét việc tìm kiếm cơ hội insite tại công ty, tham gia các studio nhỏ startup",
      },
    ];
    vm.favorite = [
      {
        img : "http://hoctiengnhatban.org/uploads/tin-tuc/2015_01/cach-doc-sach-tieng-nhat-hieu-qua.jpg",
        name: "Đọc sách",
        result: "Cà phê cùng Tony - Tony Buổi Sáng, Trên đường băng - Tony Buổi Sáng, Tử huyệt cảm xúc - RoyGarnd,...",
      },
      {
        img : "http://www.riggare.se/wp-content/uploads/2016/01/openness-research300.png",
        name: "Tìm tòi học hỏi công nghệ mới",
        result: "Swift/Objective-C, Xamarin(native/cross-platform), ...",
      },
      {
        img: "http://www.ultimate.travel/wp-content/uploads/2015/07/traveling.jpg",
        name: "Du lịch",
        result: "Đã đi phượt (tự đi) qua các điểm: Bà Rịa - Vũng Tàu, Long An, Tây Ninh - Mộc Bài - Núi Bà Đen, Suối Đá núi Dinh, ...",
      },
    ];
      vm.experience = [
        {
          img: "https://farm2.staticflickr.com/1507/26135658975_05a46e8809_n.jpg",
          title: "Meet mii (iOS) - The Simple Studio",
          content: "Sử dụng Firebase realtime database để xây dựng ứng dụng. Ứng dụng cho phép mọi người có thể kết bạn với nhau và tạo cuộc hẹn (appointment) cho nhau. Người tạo có thể chọn vị trí trên Google Map (Google Place API) và gửi cho bạn mình. Ngoài ra ứng dụng có thể được đăng nhập với các provider đang nổi hiện nay (Google, Facebook, Twiiter). Dự kiến có trên AppStore vào đầu tháng 05/2016",
        },
        {
          img: "",
          title: "Up Jump Down - Game 2D iOS - The Simple Studio",
          content: "Game 2D trên iOS sử dụng SpriteKit. Có kết nối với các mạng xã hội để chia sẽ thông tin và mời bạn bè"
        },
        {
          img: "",
          title: "Follow Me - The Simple Studio",
          content: "Ứng dụng kết nói bạn bè với vị trí(location) được GoogleMap hỗ trợ. Sử dụng Firebase làm realtime server cập nhật vị trí bạn bè với nhau."
        }
      ];

      $scope.$watchGroup(['vm.user.name', 'vm.user.email', 'vm.user.phone', 'vm.user.content'], function(n, o, scope) {
            if (n[0] !== '' && n[1] !== '' && n[2] !== '' && n[3] !== '' && n[4] !== '') {
                vm.form = false;
            }
      });


        vm.sendEmail = function(ev) {
      // Appending dialog to document.body to cover sidenav in docs app
      // Modal dialogs should fully cover application
      // to prevent interaction outside of dialog
      //key-d3051b339a2e7d8f5f3061f346584103

      // if (vm.user.email == undefined) {
      //   $mdDialog.show(
      //     $mdDialog.alert()
      //       .parent(angular.element(document.querySelector('#popupContainer')))
      //       .clickOutsideToClose(true)
      //       .title('Gửi thông tin')
      //       .textContent('email vui lòng không được để rổng')
      //       .ariaLabel('Alert Dialog Demo')
      //       .ok('Bỏ qua')
      //       .targetEvent(ev)
      //   );
      // } else {

      //}
        var href = "mailto:qhcthanh@gmail.com?subject=Message from: " + vm.user.name + "-" + vm.user.phone + "-" + vm.user.email + "&body=" + vm.user.content;
      window.location.href = href;
        vm.user.name = ''
        vm.user.phone = ''
        vm.user.email = ''
        vm.user.content = ''
            
        
      }

    }
})();