(function() {
    'use strict';

    angular
        .module('cv')
        .controller('HobbiesController', HobbiesController);

    /** @ngInject */
    function HobbiesController($timeout, $scope) {
        var vm = this;
        vm.title = "This is hobbies page";
    }
})();