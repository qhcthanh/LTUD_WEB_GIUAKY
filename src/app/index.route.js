(function() {
    'use strict';

    angular
        .module('cv')
        .config(routeConfig);

    /** @ngInject */
    function routeConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('about', {
                url: '/about',
                templateUrl: 'app/main/main.html',
                controller: 'MainController',
                controllerAs: 'vm',
                css: 'app/main/main.css'
            })
            .state('hobbies', {
                url: '/hobbies',
                templateUrl: 'app/hobbies/hobbies.html',
                controller: 'HobbiesController',
                controllerAs: 'vm'
            })
            .state('work', {
                url: '/work',
                templateUrl: 'app/work/work.html',
                controller: 'WorkController',
                controllerAs: 'vm'
            });

        $urlRouterProvider.otherwise('/about');
    }

})();